<?php
 session_start();
 $guest = (isset($_SESSION["is_guest"]) ? $_SESSION["is_guest"] : true);
 $name = htmlspecialchars($_SESSION["name"]);
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>ДЗ 2.2 Обработка форм</title>
</head>
<body>
  <p>Вы вошли как <?= ($guest ? "гость " : "администратор " )?> <strong><?= $name ?></strong></p>
  <a href="logout.php">Выйти</a>
  <hr>
<?php
  if (!$guest) {
?>  
	<h2>Загрузите новый тест</h2>
	<form action="load.php" method="post" enctype="multipart/form-data">
		<label>выберите файл</label>	
		<input type="file" name="file_name">
		<input type="submit" value="Загрузить">
	</form>
<?php } 
?>	
	<a href="list.php"> посмотреть список загруженных тестов </a>
</body>
</html>