<?php
	session_start();
	$guest = (isset($_SESSION["is_guest"]) ? $_SESSION["is_guest"] : true);
	if ($guest) {
	  header('HTTP/1.0 403 Forbidden');
	  echo "<h1>Вы вошли как гость. функция добавления теста вам не доступна </h1>";
	  echo "<a href=\"admin.php\">Вернуться к форме выбора файла</a></br>";
	  echo "<a href=\"logout.php\">Войти под другим именем</a>";
	  die();
	};
	$Path = __dir__ . "/files/";
	$test_arr = glob(__dir__ . "/files/*.json");
	if (($test_arr==false)||empty($test_arr)) {
	$file_number = 0;
	} else {
		foreach ($test_arr as &$value) {
			 $info = pathinfo($value);
			 $arr_name[] = (int)$info["filename"];
		}
	$file_number = max($arr_name);
	};
	$file_number++;

	if (empty($_FILES["file_name"])||(!$_FILES['file_name']['error'] == 0)) {
		$res = "Вы забыли выбрать файл для загрузки или что-то пошло не так. Ошибка " . $_FILES['file_name']['error'];
	} else {
		$info = pathinfo($_FILES["file_name"]["name"]);
		$ext = $info["extension"];	
		if (strcmp($ext,"json") !== 0) {
			$res = "Вы можете загрузить только файл json";    
		} else {
			$json_str = file_get_contents($_FILES["file_name"]["tmp_name"]);
			$json_obj = json_decode($json_str,true);
      if($json_obj == NULL||count($json_obj) == 0)
			{
				$res = "файл не содержит данных в нужном формате. Выберите другой файл";
			} else {
				if (move_uploaded_file($_FILES["file_name"]["tmp_name"],$Path . $file_number . ".json")) {
					$res = "Ваш тест загружен под номером " . $file_number;
					header("Location: list.php?loadnumber=" . $file_number,true);
				} else
				{
					$res = "Очень жаль, но мы не смогли загрузить Ваш файл.";
				};
			};
			
		};
	};
?>
<p>
  <h2><?= $res ?></h2>
  <a href="admin.php">Вернуться к форме выбора файла</a>
</p>