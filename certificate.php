<?php

function createCertif($name, $ball) {
	$img = @imagecreatetruecolor(800, 600);
	
	if (!$img) {
		die("Не установлена библиотека GD2");
	};
	$backGroundColor = imagecolorallocate($img, 200, 200,225);
	$fontColor = imagecolorallocate($img, 37, 44, 56);
	$pathFont = realpath(__DIR__ . "/Lobster-Regular.ttf");
	$pathBorder = realpath(__DIR__ . "/border.png");
	imagefill($img, 0, 0, $backGroundColor);
	
	$border = imagecreatefrompng($pathBorder);
	imagecopy($img, $border,0, 0, 0,0,800, 600);
	//первая строка
	$textParam = imagettfbbox ( 60 , 0 , $pathFont , "Сертификат");
	$x = (800 - $textParam[2]-$textParam[0]) / 2;
	imagettftext($img, 60, 0, $x, 100, $fontColor, $pathFont, "Сертификат");  
	//вторая
  $tmptext = $name . " выполнил тест";
	$textParam = imagettfbbox ( 20 , 0 , $pathFont , $tmptext);
	$x = (800 - $textParam[2]-$textParam[0]) / 2;
	imagettftext($img, 20, 0,  $x, 300, $fontColor, $pathFont, $tmptext);  
  //третья
  $tmptext = "Результат " . $ball . "%";
	$textParam = imagettfbbox ( 20 , 0 , $pathFont , $tmptext);
	$x = (800 - $textParam[2]-$textParam[0]) / 2;
	imagettftext($img, 20, 0,  $x, 500, $fontColor, $pathFont, $tmptext);  
	
	imagepng($img);

};

header("Content-Type: image/png");
if (isset($_GET["name"])&&isset($_GET["ball"])) {
		createCertif($_GET["name"],$_GET["ball"]);		
	}; 

