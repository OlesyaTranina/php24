<?php
$testArr = glob(__dir__ . "/files/*.json");
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задание php 22</title>
</head>
<body>
	<h2>Список загруженных тестов</h2>
	<ul>
		<?php
		foreach ($testArr as &$test){
			$info = pathinfo($test);
			?>
			<li><a href=<?= "test.php?test=" . $info["filename"] ?>>тест № <?=$info["filename"] ?></a></li>
			<?php  }
			?>
		</ul>
		<?php
		if (isset($_GET["loadnumber"])) {
			?>
			<p>Ваш тест загружен  <strong><?= "№" . $_GET["loadnumber"]  ?> </strong>
			</p>
			<?php
		}
		?>
		<a href="admin.php">Вернуться к форме выбора файла</a>
		<h3>Удалить лишний тест</h3>
		<form action="delTest.php" method="get" enctype="multipart/form-data">
			<label>Укажите номер теста </label>	
			<input type="text" name="testNumber">
			<input type="submit" value="Удалить">
		</form>
</body>
</html>