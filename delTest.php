<?php
session_start();
$guest = (isset($_SESSION["is_guest"]) ? $_SESSION["is_guest"] : true);
if ($guest) {
	  header('HTTP/1.0 403 Forbidden');
	  echo "<h1>Вы вошли как гость. функция удаления теста вам не доступна </h1>";
	  echo "<a href=\"admin.php\">Вернуться к форме выбора файла</a></br>";
	  echo "<a href=\"logout.php\">Войти под другим именем</a>";
	  die();
	};
if (isset($_GET["testNumber"])) {
		$test = $_GET["testNumber"]; 
	} else {
		header("Location: list.php",true);
	};
$Path = __dir__ . "/files/" . $test . ".json";
unlink($Path);

header("Location: list.php",true);